import 'package:flutter/material.dart';
import 'package:tugas1/home.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xFF0C356A),
        elevation: 0.0,
        title: Text(
          "Koperasi Undiksha",
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 10),
              Center(
                child: Container(
                  height: 200,
                  child: Image.asset(
                    "assets/logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              const SizedBox(height: 30),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Color(0xFF032963), // Border color
                    width: 2.0,         // Border width
                  ),
                  borderRadius: BorderRadius.circular(10), // Border radius
                ),
                child: Column(
                  children: [
                    TextFormField(
                      decoration: Tema().textInputDecoration("Username", "Enter your username"),
                    ),
                    SizedBox(height: 20.0), // Add spacing between username and password fields
                    TextFormField(
                      decoration: Tema().textInputDecoration("Password", "Enter your password"),
                      obscureText: true, // Hide the password
                    ),
                    SizedBox(height: 20.0),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                      },
                      style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30), 
                        primary: Color(0xFF0B487B),
                      ),
                      child: Text(
                        "Login",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white, 
                        ),
                      ),
                    ),
                    const SizedBox(height: 40),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                          onPressed: () {
                            // Add logic for "Forgot Password" button here
                          },
                          child: Text(
                            "Daftar",
                            style: TextStyle(color: Color(0xFF0C356A)),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            // Add logic for "Forgot Password" button here
                          },
                          child: Text(
                            "Lupa Password",
                            style: TextStyle(color: Color(0xFF0C356A)),
                          ),
                        ),
                        
                      ],
                    ),
                  ],
                ),
              ),
              
            ]
          ),
        ),
        
      ),
      bottomNavigationBar: Container(
          color: Color(0xFF0C356A), // Footer background color
          padding: EdgeInsets.all(10.0), // Footer padding
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "copyright @2023 Undiksha", // Footer text
                style: TextStyle(
                  color: Colors.white, // Text color
                ),
              ),
            ],
          ),
        ),
    );
  }
}

class Tema {
  InputDecoration textInputDecoration([String lableText="", String hintText = ""]){
    return InputDecoration(
      labelText: lableText,
      hintText: hintText,
      fillColor: Colors.white,
      filled: true,
      contentPadding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(7.0), borderSide: BorderSide(color: Colors.grey)),
      enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(7.0), borderSide: BorderSide(color: Colors.grey.shade400)),
      errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(7.0), borderSide: BorderSide(color: Colors.red, width: 2.0)),
      focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(7.0), borderSide: BorderSide(color: Colors.red, width: 2.0)),
    );
  }

  BoxDecoration inputBoxDecorationShaddow() {
    return BoxDecoration(boxShadow: [
      BoxShadow(
        color: Colors.black.withOpacity(0.07),
        blurRadius: 20,
        offset: const Offset(0, 5),
      )
    ]);
  }
}