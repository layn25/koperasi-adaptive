import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Color(0xFF0C356A),
          elevation: 0.0,
          title: Text(
            "Koperasi Undiksha",
            textAlign: TextAlign.center,
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          actions: [
            IconButton(
              icon: const Icon(
                Icons.logout,
                color: Colors.white,
              ),
              onPressed: () {},
            ),
          ],
        ),
        body: Center(
          child: LayoutBuilder(
            builder: (BuildContext, BoxConstraints constraints) {
              if (constraints.maxWidth < 600) {
                return const Mobile();
              }
              else {
                return const Dasktop();
              }
            }
          ),
        ),
        bottomNavigationBar: Container(
          height: 80,
          color: const Color(0xFF0C356A),
          padding: EdgeInsets.fromLTRB(20, 2, 20, 2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: const Icon(
                  Icons.settings,
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {},
              ),
              InkWell(
                onTap: () {
                  // Tambahkan logika ketika tombol "Scan" ditekan di sini
                },
                borderRadius: BorderRadius.circular(40),
                child: Container(
                  width: 80,
                  height: 80,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle, 
                    color: Colors.white, 
                  ),
                  child: Center(
                    child: Icon(
                      Icons.qr_code, 
                      color: const Color(0xFF0C356A), 
                      size: 40,
                    ),
                  ),
                ),
              ),
              IconButton(
                icon: const Icon(
                  Icons.account_circle_rounded,
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {},
              ),
            ],
          ),
        ),
      )
    );
  }
}

class Mobile extends StatelessWidget {
  const Mobile({super.key});
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [ 
          Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xFF0C356A), // Border color
                width: 2.0,         // Border width
              ),
              borderRadius: BorderRadius.circular(10), // Border radius
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 140,
                  width: 140,
                  child: Image.asset(
                    "assets/profil.jpg",
                    fit: BoxFit.contain,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 180,
                        margin: EdgeInsets.only(bottom: 10),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Color(0xFF91C8E4),
                          borderRadius: BorderRadius.circular(10)
                        ),
                        child: Column(
                          children: [
                            Text(
                              "Nama",
                              style: TextStyle(
                                fontSize: 18,
                                color: Color(0xFFFFFFFF), 
                                fontWeight: FontWeight.bold,
                              )
                            ),
                            Text(
                              "Putu Ryan Darmayasa",
                              style: TextStyle(
                                fontSize: 14,
                                color: Color(0xFFFFFFFF), 
                              )
                            ),
                          ]
                        ),
                      ),
                      Container(
                        width: 180,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Color(0xFF91C8E4), 
                          borderRadius: BorderRadius.circular(10)
                        ),
                        child: Column(
                          children: [
                            Text(
                              "Total Saldo",
                              style: TextStyle(
                                fontSize: 18,
                                color: Color(0xFFFFFFFF),
                                fontWeight: FontWeight.bold, 
                              )
                            ),
                            Text(
                              "Rp. 100000",
                              style: TextStyle(
                                fontSize: 14,
                                color: Color(0xFFFFFFFF), 
                              )
                            ),
                          ]
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xFF0C356A), // Border color
                width: 2.0,         // Border width
              ),
              borderRadius: BorderRadius.circular(10), // Border radius
            ),
            child: GridView.count(
              // padding: EdgeInsets.only(left: 10, right: 10),
              crossAxisCount: 3,
              childAspectRatio: 1.0,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10, 
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.account_balance_wallet,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Dompet",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.currency_exchange_outlined,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Tranrsfer",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.attach_money,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Deposite",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.credit_card,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Pembayaran",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.money,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Pinjaman",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.wallet,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Investasi",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 40),
          Container(
            padding: EdgeInsets.all(20),
            color: Color(0xFF0C356A),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Butuh Bantuan",
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFFFFFFFF),
                          fontWeight: FontWeight.bold, 
                        )
                      ),
                      Text(
                        "0827494934948",
                        style: TextStyle(
                          fontSize: 30,
                          color: Color(0xFFFFFFFF),
                          fontWeight: FontWeight.bold, 
                        )
                      ),
                    ],
                  ),
                  SizedBox(width: 20.0),
                  Icon(
                    Icons.call,
                    color: Colors.white,
                    size: 60,
                  ),
                ],
              ),
            )
          )
        ],
      ),
    );
  }
}

class Dasktop extends StatelessWidget {
  const Dasktop ({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [ 
          Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xFF0C356A), // Border color
                width: 2.0,         // Border width
              ),
              borderRadius: BorderRadius.circular(10), // Border radius
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 120,
                  width: 120,
                  
                  child: Image.asset(
                    "assets/profil.jpg",
                    fit: BoxFit.contain,
                  ),
                ),
                Container(
                  width: 300,
                  height: 120,
                  margin: EdgeInsets.only(left: 10, right: 10),
                  padding: EdgeInsets.only(left: 50, right: 50, top: 35),
                  decoration: BoxDecoration(
                    color: Color(0xFF91C8E4),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    children: [
                      Text(
                        "Nama",
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFFFFFFFF), 
                          fontWeight: FontWeight.bold,
                        )
                      ),
                      Text(
                        "Putu Ryan Darmayasa",
                        style: TextStyle(
                          fontSize: 14,
                          color: Color(0xFFFFFFFF), 
                        )
                      ),
                    ]
                  ),
                ),
                Container(
                  width: 280,
                  height: 120,
                  padding: EdgeInsets.only(left: 50, right: 50, top: 35),
                  decoration: BoxDecoration(
                    color: Color(0xFF91C8E4),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    children: [
                      Text(
                        "Total Saldo",
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFFFFFFFF),
                          fontWeight: FontWeight.bold, 
                        )
                      ),
                      Text(
                        "Rp. 100000",
                        style: TextStyle(
                          fontSize: 14,
                          color: Color(0xFFFFFFFF), 
                        )
                      ),
                    ]
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xFF0C356A), // Border color
                width: 2.0,         // Border width
              ),
              borderRadius: BorderRadius.circular(10), // Border radius
            ),
            child: GridView.count(
              // padding: EdgeInsets.only(left: 10, right: 10),
              crossAxisCount: 5,
              childAspectRatio: 1.0,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10, 
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.account_balance_wallet,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Dompet",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.currency_exchange_outlined,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Tranrsfer",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.attach_money,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Deposite",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.credit_card,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Pembayaran",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.money,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Pinjaman",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    decoration: BoxDecoration(
                      color: Color(0xFF91C8E4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.wallet,
                          color: Colors.white,
                          size: 60,
                        ),
                        Text(
                          "Investasi",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFFFFFFF),
                          )
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 40),
          Container(
            padding: EdgeInsets.all(20),
            color: Color(0xFF0C356A),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Butuh Bantuan",
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFFFFFFFF),
                          fontWeight: FontWeight.bold, 
                        )
                      ),
                      Text(
                        "0827494934948",
                        style: TextStyle(
                          fontSize: 30,
                          color: Color(0xFFFFFFFF),
                          fontWeight: FontWeight.bold, 
                        )
                      ),
                    ],
                  ),
                  SizedBox(width: 20.0),
                  Icon(
                    Icons.call,
                    color: Colors.white,
                    size: 60,
                  ),
                ],
              ),
            )
          )
        ],
      ),
    );
  }
}